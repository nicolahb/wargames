package edu.ntnu.idatt2001.nicolahb;
import edu.ntnu.idatt2001.nicolahb.gui.App;


/**
 * Class Main
 * Runs the program and is called on by default when launching the program.
 * @author Nicolai Brand.
 * @version 05.05.2022
 */
public class Main {

    public static void main(String[] args) {
        App.main();
    }
}
