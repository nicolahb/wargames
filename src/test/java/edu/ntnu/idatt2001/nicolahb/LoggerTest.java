package edu.ntnu.idatt2001.nicolahb;

import edu.ntnu.idatt2001.nicolahb.gui.models.Logger;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LoggerTest {

    @Nested
    public class InitLogger {

        @Test
        public void loggerWithoutReverseFlagDoesNotAddItemsAtIndexZero() {
            Logger<String> log = new Logger<>();

            log.addLogItem("First");
            log.addLogItem("Second");

            assertNotEquals("Second", log.getLog().get(0));
            assertEquals("First", log.getLog().get(0));
        }


        @Test
        public void loggerWithReverseFlagAddsItemsAtIndexZero() {

            Logger<String> log = new Logger<>(true);

            log.addLogItem("First");
            log.addLogItem("Second");

            assertEquals("Second", log.getLog().get(0));
        }
    }

    @Nested
    public class ClearLog {

        @Test
        public void clearingLogWithElementsIsEmpty() {
            Logger<String> log = new Logger<>();
            log.addLogItem("A");
            log.addLogItem("B");
            assertFalse(log.getLog().isEmpty());
            log.clearLog();
            assertTrue(log.getLog().isEmpty());
        }
    }
}
