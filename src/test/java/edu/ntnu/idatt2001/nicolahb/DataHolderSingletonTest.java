package edu.ntnu.idatt2001.nicolahb;

import edu.ntnu.idatt2001.nicolahb.gui.models.DataHolderSingleton;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DataHolderSingletonTest {

    @Nested
    public class InitDataHolderSingleton {

        /* Nothing more can be tested as it's dependent on JavaFX running */
        @Test
        public void getInstanceIsAlwaysEqual() {

            DataHolderSingleton first = DataHolderSingleton.getDataHolderSingleton();
            DataHolderSingleton second = DataHolderSingleton.getDataHolderSingleton();

            assertEquals(first, second);
        }

    }
}
